package com.example.practicalassignment

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.practicalassignment.repository.MockPriceRepositoryImpl
import com.example.practicalassignment.ui.main.MainViewModel
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4


@RunWith(JUnit4::class)
class MainViewModelTest {
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private val priceRepository = MockPriceRepositoryImpl()
    private val mainViewModel = MainViewModel(priceRepository)

    @Test
    fun testIncreaseDesiredPrice() {
        mainViewModel.setDesiredPrice("100.00")
        mainViewModel.increaseDesiredPrice()
        assertEquals(101.00, mainViewModel.desiredPrice.value)
    }

    @Test
    fun testDecreaseDesiredPrice() {
        mainViewModel.setDesiredPrice("100.00")
        mainViewModel.decreaseDesiredPrice()
        assertEquals(99.00, mainViewModel.desiredPrice.value)
    }
}
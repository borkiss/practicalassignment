package com.example.practicalassignment

import com.example.practicalassignment.data.Price
import com.example.practicalassignment.repository.PriceRepository
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations


class MockPriceRepositoryImplTest : BaseRxTest() {

    @Mock
    private lateinit var priceRepository: PriceRepository

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun testObserving() {
        Mockito.`when`(priceRepository.getPrice())
            .thenReturn(Observable.just(Price(0.5, 0.6), Price(0.8, 0.95)))

        val testObserver = priceRepository.getPrice()
            .test()

        testObserver.assertValues(Price(0.5, 0.6), Price(0.8, 0.95))
        testObserver.dispose()
    }
}
package com.example.practicalassignment.repository

import com.example.practicalassignment.data.Price
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import kotlin.random.Random

class MockPriceRepositoryImpl: PriceRepository {
    private val random = Random(System.currentTimeMillis())

    override fun getPrice(): Observable<Price> {
        return Observable.interval(2, 10, TimeUnit.SECONDS)
            .map {
                Price(
                    buy = random.nextDouble(0.0, 1.0),
                    sell = random.nextDouble(0.0, 1.0)
                )
            }
    }
}
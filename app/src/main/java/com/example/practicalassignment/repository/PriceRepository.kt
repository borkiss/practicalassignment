package com.example.practicalassignment.repository

import com.example.practicalassignment.data.Price
import io.reactivex.Observable

interface PriceRepository {
    fun getPrice(): Observable<Price>
}
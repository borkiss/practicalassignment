package com.example.practicalassignment.ext

import android.widget.Button

fun Button.getButtonText(): String =
    this.text.toString()
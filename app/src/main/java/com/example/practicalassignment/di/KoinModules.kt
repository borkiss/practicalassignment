package com.example.practicalassignment.di

import com.example.practicalassignment.repository.MockPriceRepositoryImpl
import com.example.practicalassignment.repository.PriceRepository
import com.example.practicalassignment.ui.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val repositoryModule = module {
    single<PriceRepository> { MockPriceRepositoryImpl() }
}

val viewModelModule = module {
    viewModel { MainViewModel(priceRepository = get()) }
}
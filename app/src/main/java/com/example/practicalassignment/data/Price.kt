package com.example.practicalassignment.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Price (
    val buy: Double,
    val sell: Double
) : Parcelable
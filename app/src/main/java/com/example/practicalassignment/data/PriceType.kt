package com.example.practicalassignment.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
enum class PriceType : Parcelable {
    BUY, SELL
}
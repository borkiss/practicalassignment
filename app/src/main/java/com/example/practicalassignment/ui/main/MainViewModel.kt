package com.example.practicalassignment.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.practicalassignment.data.Price
import com.example.practicalassignment.data.PriceType
import com.example.practicalassignment.data.PriceType.BUY
import com.example.practicalassignment.repository.PriceRepository
import com.example.practicalassignment.ui.main.ViewState.*
import io.reactivex.disposables.Disposable

class MainViewModel(val priceRepository: PriceRepository) : ViewModel() {

    private var disposable: Disposable? = null

    private val _price = MutableLiveData<ViewState<Price>>()
    val price: LiveData<ViewState<Price>> = _price

    val desiredPrice = MutableLiveData<Double?>()
    val priceType = MutableLiveData<PriceType>()

    init {
        priceType.postValue(BUY)
        getData()
    }

    fun getData() {
        disposable = priceRepository.getPrice()
            .doOnSubscribe {
                _price.postValue(Loading())
            }
            .subscribe( {
                _price.postValue(Success(it))
            }, {
                _price.postValue(Error(it))
            })
    }

    fun setPriceType(priceType: PriceType) {
        this.priceType.postValue(priceType)
    }

    fun setDesiredPrice(price: String?) {
        desiredPrice.postValue(price?.toDoubleOrNull())
    }

    fun increaseDesiredPrice() {
        val oldPrice = getOldPrice()
        val newPrice = oldPrice?.plus((oldPrice / 100))

        desiredPrice.postValue(newPrice)
    }

    fun decreaseDesiredPrice() {
        val oldPrice = getOldPrice()
        val newPrice = oldPrice?.minus((oldPrice / 100))

        desiredPrice.postValue(newPrice)
    }

    private fun getOldPrice(): Double? {
        return if (desiredPrice.value != null) {
            desiredPrice.value
        } else {
            when(_price.value) {
                is Success -> {
                    (_price.value as Success<Price>).data.let {
                        if (priceType.value == BUY) {
                            it.buy
                        } else {
                            it.sell
                        }
                    }
                }
                else -> {
                    null
                }
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.dispose()
    }
}

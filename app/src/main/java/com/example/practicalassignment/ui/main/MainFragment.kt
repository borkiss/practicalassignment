package com.example.practicalassignment.ui.main

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.practicalassignment.R
import com.example.practicalassignment.data.Price
import com.example.practicalassignment.data.PriceType
import com.example.practicalassignment.data.PriceType.BUY
import com.example.practicalassignment.data.PriceType.SELL
import com.example.practicalassignment.databinding.MainFragmentBinding
import com.example.practicalassignment.ext.getButtonText
import com.google.android.material.snackbar.Snackbar
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
        private const val EXTRA_CURRENT_PRICE = "currentPrice"
        private const val EXTRA_CURRENT_PRICE_TYPE = "currentPriceType"
        private const val EXTRA_PRICE_CHANGED_FROM_VIEW = "changedFromView"
    }

    private val viewModel: MainViewModel by viewModel()
    private lateinit var binding: MainFragmentBinding

    private lateinit var currentPrice: Price
    private var currentPriceType = BUY
    // fix infinity loop in EditText desiredPrice
    private var priceChangedFromView = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(inflater)
        return binding.root
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(EXTRA_CURRENT_PRICE, currentPrice)
        outState.putParcelable(EXTRA_CURRENT_PRICE_TYPE, currentPriceType)
        outState.putBoolean(EXTRA_PRICE_CHANGED_FROM_VIEW, priceChangedFromView)
        super.onSaveInstanceState(outState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        savedInstanceState?.let {
            currentPrice = it.getParcelable<Price>(EXTRA_CURRENT_PRICE) as Price
            currentPriceType = it.getParcelable<PriceType>(EXTRA_CURRENT_PRICE_TYPE) as PriceType
            priceChangedFromView = it.getBoolean(EXTRA_PRICE_CHANGED_FROM_VIEW)
        }

        viewModel.price.observe(viewLifecycleOwner, Observer {
            setViewState(it)
        })

        viewModel.desiredPrice.observe(viewLifecycleOwner, Observer {
            if (!priceChangedFromView) {
                binding.desiredPrice.setText(it.toString())
                priceChangedFromView = false
            }
            setHint()
        })

        viewModel.priceType.observe(viewLifecycleOwner, Observer {
            currentPriceType = it
        })

        setupView()
    }

    private fun setupView() {
        binding.plusButton.setOnClickListener {
            priceChangedFromView = false
            viewModel.increaseDesiredPrice()
            setHint()
        }

        binding.minusButton.setOnClickListener {
            priceChangedFromView = false
            viewModel.decreaseDesiredPrice()
            setHint()
        }

        binding.applyButton.setOnClickListener {
            showMessage((it as Button).getButtonText())
        }

        binding.cancelButton.setOnClickListener {
            showMessage((it as Button).getButtonText())
        }

        binding.priceTypeRadioGroup.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.radioSell -> {
                    viewModel.setPriceType(SELL)
                    currentPriceType = SELL
                }
                R.id.radioBuy -> {
                    viewModel.setPriceType(BUY)
                    currentPriceType = BUY
                }
            }
            setCurrentRate()
            setHint()
        }

        binding.radioDeliveryByEmail.setOnClickListener {
            showMessage((it as Button).getButtonText())
        }

        binding.radioDeliveryBySMS.setOnClickListener {
            showMessage((it as Button).getButtonText())
        }

        binding.radioDeliveryByPush.setOnClickListener {
            showMessage((it as Button).getButtonText())
        }

        binding.desiredPrice.addTextChangedListener(object : TextWatcher {
            var beforeText: String? = null

            override fun afterTextChanged(s: Editable?) {
                if (beforeText != s.toString()) {
                    priceChangedFromView = true
                    viewModel.setDesiredPrice(s?.toString())
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                beforeText = s.toString()
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
    }

    private fun setViewState(viewState: ViewState<Price>) =
        when (viewState) {
            is ViewState.Loading -> {
                binding.main.visibility = GONE
                binding.loading.root.visibility = VISIBLE
                binding.error.root.visibility = GONE
            }
            is ViewState.Success -> {
                currentPrice = viewState.data
                setCurrentRate()
                setHint()

                binding.main.visibility = VISIBLE
                binding.loading.root.visibility = GONE
                binding.error.root.visibility = GONE
            }
            is ViewState.Error -> {
                binding.main.visibility = GONE
                binding.loading.root.visibility = GONE
                binding.error.root.visibility = VISIBLE
            }
        }

    private fun setCurrentRate() {
        binding.currentRate.text = when (currentPriceType) {
            BUY -> {
                getString(R.string.current_buy_rate, currentPrice.buy)
            }
            SELL -> {
                getString(R.string.current_sell_rate, currentPrice.sell)
            }
        }
    }

    private fun setHint() {
        val price = binding.desiredPrice.text.toString().toDoubleOrNull()

        if (price == null || price == 0.0) {
            binding.percentHint.text = ""
        } else {

            val currentPrice: Double = this.currentPrice.let {
                if (currentPriceType == BUY) {
                    it.buy
                } else {
                    it.sell
                }
            }
            val percent = (price - currentPrice) / price * 100

            binding.percentHint.text = if (percent > 0) {
                getString(R.string.above_price, percent)
            } else {
                getString(R.string.below_price, -percent)
            }
        }
    }

    private fun showMessage(message: String) {
        Snackbar.make(binding.main, message, Snackbar.LENGTH_SHORT).show()
    }
}

# Practical Assignment
### Notes
* The service for prices generates prices in range from 0.0 to 1.0. The first value emits after 2 ceconds, all another values after 10 seconds.
* Since value of a price is not integer then increasing or decreasing by 1% may gives a little bit more or less value than 1%.
The price may be changed by buttons + and - or manually in edit text field.
* For selecting Delivery method I have used Radio group with ```android:button="@null"``` param in layout. But later I detected that it shows radio dots with text on emulators with API17 and API19 (Windows version). I investigated it with the same API version with emulators on MacOS, and it was fine, without dots. Unfortunatelly I haven't any real device with these API versions to check it. I hope it not too influences on your final descision.
